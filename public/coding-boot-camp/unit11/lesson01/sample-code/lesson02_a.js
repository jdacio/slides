var dogs = {
  raining: true,
  noise: 'Woof!',
  makeNoise: function() {
    if (this.raining === true) {
      console.log(this.noise);
    }
  }
};

var cats = {
  raining: false,
  noise: 'Meow!',
  makeNoise: function() {
    if (this.raining === true) {
      console.log(this.noise);
    }
  }
};

var bears = {
  raining: false,
  noise: 'Raaawwwwrr!',
  makeNoise: function() {
    if (this.raining === true) {
      console.log(this.noise);
    }
  }
};

var tarantulas = {
  raining: true,
  noise: null,
  makeNoise: function() {
    if (this.raining === true) {
      console.log(this.noise);
    }
  }
};

var iguanas = {
  raining: false,
  noise: null,
  makeNoise: function() {
    if (this.raining === true) {
      console.log(this.noise);
    }
  }
};

var dragons = {
  raining: false,
  noise: 'RAAAAAAWWRRRR!!!🔥🔥🔥🔥',
  makeNoise: function() {
    if (this.raining === true) {
      console.log(this.noise);
    }
  }
};

console.log(dogs)
console.log(cats)
console.log(bears)
console.log(tarantulas)
console.log(iguanas)
console.log(dragons)
