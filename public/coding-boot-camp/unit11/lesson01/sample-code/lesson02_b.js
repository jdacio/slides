function SomeConstructor(keyA, keyB, keyC) {
  this.keyA = keyA;
  this.keyB = keyB;
  this.keyC = keyC;
}

var someObjectA = new SomeConstructor(4, "apple", false)
var someObjectB = new SomeConstructor(1, "watermelon", false)
var someObjectC = new SomeConstructor(42, "grape", false)
var someObjectD = new SomeConstructor(13, "durian", true)
var someObjectE = new SomeConstructor(237, "mango", false)
var someObjectF = new SomeConstructor(88, "apricot", true)
var someObjectG = new SomeConstructor(100, "banana", true)
var someObjectH = new SomeConstructor(77, "kiwi", false)

console.log(someObjectA)
console.log(someObjectB)
console.log(someObjectC)
console.log(someObjectD)
console.log(someObjectE)
console.log(someObjectF)
console.log(someObjectG)
console.log(someObjectH)