function Animal(raining, noise) {
  this.raining = raining;
  this.noise = noise;
  this.makeNoise = function() {
    if (this.raining === true) {
      console.log(this.noise);
    }
  };
}

var dogs = new Animal(true, 'Woof!');
var cats = new Animal(true, 'Meow!');
var bears = new Animal(true, 'Raaawwwwrr!');
var tarantulas = new Animal(true, null);
var iguanas = new Animal(true, null);
var dragons = new Animal(true, 'RAAAAAAWWRRRR!!!🔥🔥🔥🔥');


var makeRuckus = function(dogs, cats, bears, tarantulas, iguanas, dragons) {
    dogs.makeNoise();
    cats.makeNoise();
    bears.makeNoise();
    tarantulas.makeNoise();
    iguanas.makeNoise();
    dragons.makeNoise();
}

makeRuckus(dogs, cats, bears, tarantulas, iguanas, dragons);
